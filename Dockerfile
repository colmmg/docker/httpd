FROM amazonlinux:latest 

RUN yum install -y httpd
RUN echo "Hello World!" > /var/www/html/index.html

COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh

ENTRYPOINT ["/root/entrypoint.sh"]

EXPOSE 80